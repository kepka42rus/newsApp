<?php

namespace App\Http\Controllers;

use App\Models\News;


class NewsController extends Controller
{
    public function getNews()
    {
        if (request('count')) {
            $news = News::paginate(request('count'));
        } else {
            $news = News::paginate(10);
        }

        $news->makeHidden('body');
        return $news->toJson();
    }

    public function getNewsById($id)
    {
        $news = News::find($id);
        return response()->json($news);
    }
}