<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            App\Models\News::create([
                'title' => str_random(10),
                'description' => str_random(100),
                'body' => str_random(200)
            ]);
        }

    }
}
